#include <iostream>
#include <cmath>
#include <SFML/Graphics.hpp>
#include <STP/TMXLoader.hpp>

float Lerp(const float& goal, const float& current, const float dt) {
    float difference = goal - current;

    if (difference > dt)
        return current + dt;
    if (difference < -dt)
        return current - dt;

    return goal;
}

int main() {
    sf::Vector2f windowSize(800, 600);
    sf::RenderWindow window(sf::VideoMode(windowSize.x, windowSize.y), "The Quest");
    window.setFramerateLimit(60);

    sf::View* view = new sf::View();

    tmx::TileMap* map = new tmx::TileMap("maps/newstart.tmx");
    map->ShowObjects();

    sf::Texture dudeTexture;
    if (!dudeTexture.loadFromFile("assets/dude.png")) {
        std::cout << "Unable to load 'assets/dude.png'" << std::endl;
        return 0;
    }

    sf::Sprite* dude = new sf::Sprite;
    dude->setTexture(dudeTexture);
    dude->setPosition(sf::Vector2f(240.f, 368.f));

    sf::Vector2f vecVelocityGoal;
    vecVelocityGoal.x = 0;
    vecVelocityGoal.y = 0;
    sf::Vector2f vecVelocity;
    vecVelocity.x = 0;
    vecVelocity.y = 0;

    bool dbgShowBox = false;

    sf::Clock clock;
    while (window.isOpen()) {
        sf::Time deltaTime = clock.restart();
        sf::FloatRect dudeBox = dude->getGlobalBounds();

        view->setCenter(dude->getPosition());
        view->setSize(400, 300);
        window.setView(*(view));

        window.clear();
        window.draw(*(map));
        window.draw(*(dude));
        if (dbgShowBox) {
            sf::Vertex line[] = {
                sf::Vertex(sf::Vector2f(dudeBox.left, dudeBox.top)),
                sf::Vertex(sf::Vector2f((dudeBox.left + dudeBox.width), dudeBox.top)),
                sf::Vertex(sf::Vector2f((dudeBox.left + dudeBox.width), (dudeBox.top + dudeBox.height))),
                sf::Vertex(sf::Vector2f(dudeBox.left, (dudeBox.top + dudeBox.height))),
                sf::Vertex(sf::Vector2f(dudeBox.left, dudeBox.top))
            };
            window.draw(line, 5, sf::LineStrip);
        }
        window.display();

        sf::Event event;

        while (window.pollEvent(event)) {
            switch (event.type) {
                case sf::Event::Closed:
                    window.close();
                    break;
                case sf::Event::LostFocus:
                    vecVelocityGoal.x = 0;
                    vecVelocityGoal.y = 0;
                    break;
                case sf::Event::KeyPressed:
                    if (event.key.code == sf::Keyboard::Escape) {
                        window.close();
                    }
                    if (event.key.code == sf::Keyboard::F5) {
                        dbgShowBox = !dbgShowBox;
                    }
                    if (event.key.code == sf::Keyboard::F6) {
                        std::cout << "Reloading map." << std::endl;
                        delete map;
                        map = new tmx::TileMap("maps/newstart.tmx");

                        std::cout << "Done." << std::endl;
                    }
                    if (event.key.code == sf::Keyboard::W) {
                        vecVelocityGoal.y = -55;
                    }
                    if (event.key.code == sf::Keyboard::A) {
                        vecVelocityGoal.x = -55;
                    }
                    if (event.key.code == sf::Keyboard::S) {
                        vecVelocityGoal.y = 55;
                    }
                    if (event.key.code == sf::Keyboard::D) {
                        vecVelocityGoal.x = 55;
                    }
                    break;
                case sf::Event::KeyReleased:
                    if (event.key.code == sf::Keyboard::W) {
                        vecVelocityGoal.y = 0;
                    }
                    if (event.key.code == sf::Keyboard::A) {
                        vecVelocityGoal.x = 0;
                    }
                    if (event.key.code == sf::Keyboard::S) {
                        vecVelocityGoal.y = 0;
                    }
                    if (event.key.code == sf::Keyboard::D) {
                        vecVelocityGoal.x = 0;
                    }
                    break;
                case sf::Event::Resized:
                    windowSize.x = event.size.width;
                    windowSize.y = event.size.height;
                    view->setSize(windowSize);
                    window.setView(*(view));
                    break;
                default:
                    break;
            }
        }

        vecVelocity.x = Lerp(vecVelocityGoal.x, vecVelocity.x, deltaTime.asSeconds() * 350);
        vecVelocity.y = Lerp(vecVelocityGoal.y, vecVelocity.y, deltaTime.asSeconds() * 350);

        sf::Vector2f vecPosition = dude->getPosition();
        dude->setPosition(vecPosition + vecVelocity * deltaTime.asSeconds());
        std::cout << "Delta time: " << deltaTime.asMilliseconds() << "\n";
    }
    delete view;
    delete map;
    delete dude;
    
    return 0;
}
