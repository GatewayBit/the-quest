main : main.o
	g++ -Wall -std=c++11 -lsfml-graphics -lsfml-window -lsfml-system -lSTP-d -o game main.o

main.o : main.cpp 
	g++ -g -c main.cpp

clean : 
	rm game main.o 
